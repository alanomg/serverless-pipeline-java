variable "region" {
  default = "eu-west-1"
  type    = string
}

variable "function_name" {
  type        = string
  description = "Function name"
  default     = "a-funky-function"
}

# "node" is the filename within the zip file (node.js) and "handler"
# is the name of the property under which the handler function was
# exported in that file.
variable "handler" {
  type        = string
  description = "The handler for the serverless invocation"
  default     = "the-app/node.handler"
}

variable "runtime" {
  type        = string
  description = "The handler runtime for serverless invocation"
  default     = "nodejs10.x"
}

variable "s3_bucket" {
  type        = string
  description = "The S3 bucket that holds the packaged function"
  default     = "funky-function-bucket"
}

variable "s3_key" {
}